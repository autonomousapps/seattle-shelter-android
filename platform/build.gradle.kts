plugins {
    id("java-platform")
}

dependencies {
    constraints {
        api("androidx.annotation:annotation:1.1.0-rc01")
        api("androidx.appcompat:appcompat:1.1.0-rc01")
        api("androidx.constraintlayout:constraintlayout:2.0.0-beta2")
        api("androidx.core:core-ktx:1.0.1")
        api("androidx.recyclerview:recyclerview:1.0.0")
        api("com.google.android.material:material:1.0.0-rc01")
        api("com.squareup.picasso:picasso:2.71828")
        api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.50")

        // Tests
        api("junit:junit:4.12")

        // Android tests
        api("androidx.test:runner:1.1.0")
        api("androidx.test.espresso:espresso-core:3.1.0")
    }
}