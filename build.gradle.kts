import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.autonomousapps.dependency-analysis") version "0.71.0"
}

subprojects {
    repositories {
        google()
        mavenCentral()
        jcenter()
    }
    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

tasks.register("sanityCheck") {
    dependsOn(":app:compileDebugSources")
}
