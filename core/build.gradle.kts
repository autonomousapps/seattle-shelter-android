plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {
    implementation(platform(project(":platform")))

    implementation(project(":db"))
    implementation(project(":entities"))

    dagger()
    archX()
    rxJava()

    // old retrofit()
    implementation(Deps.moshi)
    implementation(Deps.moshiAdapters)
    implementation(Deps.retrofit)
    implementation(Deps.retrofitConverterMoshi) // compileOnly?
    implementation(Deps.retrofitAdapterRxJava)
    implementation(Deps.okhttp)
    implementation(Deps.okhttpLogging)
    implementation(Deps.okio)

    implementation("androidx.annotation:annotation")
//    compileOnly("androidx.annotation:annotation")

    implementation("androidx.appcompat:appcompat")
    api("androidx.constraintlayout:constraintlayout")
    implementation("androidx.core:core-ktx")
    implementation("androidx.recyclerview:recyclerview")
    implementation("com.google.android.material:material")
    implementation("com.squareup.picasso:picasso") {
        exclude(module = "exifinterface")
    }
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
