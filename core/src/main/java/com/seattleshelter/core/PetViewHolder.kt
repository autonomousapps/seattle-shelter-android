package com.seattleshelter.core

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.seattleshelter.entities.Pet
import com.squareup.picasso.Picasso

class PetViewHolder(
    itemView: View,
    private val clickListener: (Long) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private val name: TextView by lazy { itemView.findViewById<TextView>(R.id.name) }
    private val sex: TextView by lazy { itemView.findViewById<TextView>(R.id.sex) }
    private val breed: TextView by lazy { itemView.findViewById<TextView>(R.id.breed) }
    private val age: TextView by lazy { itemView.findViewById<TextView>(R.id.age) }
    private val foster: TextView by lazy { itemView.findViewById<TextView>(R.id.foster) }
    private val adoptionPending: TextView by lazy { itemView.findViewById<TextView>(R.id.adoptionPending) }
    private val imageView: ImageView by lazy { itemView.findViewById<ImageView>(R.id.imageView) }

    @SuppressLint("SetTextI18n")
    fun bind(pet: Pet) {
        name.text = pet.name
        sex.text = pet.sex
        breed.text = pet.breed
        age.text = if (pet.age.isNotEmpty()) "${pet.age} old" else "Age unknown"
        if (pet.inFosterCare) {
            foster.text = "In foster care"
            foster.visibility = View.VISIBLE
        } else {
            foster.visibility = View.GONE
        }
        if (pet.adoptionPending) {
            adoptionPending.text = "Adoption pending"
            adoptionPending.visibility = View.VISIBLE
        } else {
            adoptionPending.visibility = View.GONE
        }

        Picasso.get().load(pet.picUrl).into(imageView)
        imageView.setOnClickListener { clickListener(pet.id) }
    }
}
