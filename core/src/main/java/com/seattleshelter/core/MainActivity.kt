package com.seattleshelter.core

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Spinner
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seattleshelter.core.api.PetsService
import com.seattleshelter.core.base.BaseActivity
import com.seattleshelter.db.daos.PetDao
import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetType
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject lateinit var vmFactory: MainActivityViewModelFactory
    private val viewModel by lazy(mode = LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this, vmFactory).get(PetsViewModel::class.java)
    }

    private val petAdapter by lazy { PetAdapter { id -> openPetDetails(id) } }

    private val recyclerView by lazy { findViewById<RecyclerView>(R.id.recyclerView) }
    private val petsSpinner by lazy { findViewById<Spinner>(R.id.petsSpinner) }
    private val progressBar by lazy { findViewById<ContentLoadingProgressBar>(R.id.progressBar) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        initSpinner()
        initRecyclerView()

        viewModel.petsList().observe(this, Observer { pets ->
            pets?.let {
                Log.v(TAG, "Observed pets! Count = ${pets.size}")
                handlePets(it)
            }
        })
    }

    private fun initSpinner() {
        with(petsSpinner) {
            onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                    val petType = PetType.fromString(parent.getItemAtPosition(position) as String)
                    Log.d(TAG, "Selected $petType")
                    viewModel.setPetType(petType)
                    progressBar.show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    Log.d(TAG, "Nothing selected")
                }
            }
        }
    }

    private fun initRecyclerView() {
        with(recyclerView) {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = petAdapter
        }
    }

    private fun handlePets(petsList: List<Pet>) {
        petAdapter.setPets(petsList)
        handleProgressBar(petsList.isEmpty())
    }

    private fun handleProgressBar(isPetsListEmpty: Boolean) {
        progressBar.hide()
        if (isPetsListEmpty) {
            // TODO hide RV and show message saying no pets of this type
            recyclerView.visibility = View.INVISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
        }
    }

    private fun openPetDetails(id: Long) {
        startActivity(PetDetailActivity.getStartIntent(this, id))
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}

class MainActivityViewModelFactory @Inject constructor(
    private val petDao: PetDao,
    private val petsService: PetsService
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(PetsViewModel::class.java) -> {
                PetsViewModel(petDao, petsService) as T
            }
            else -> throw IllegalArgumentException(
                "${modelClass.simpleName} is an unknown type of view model"
            )
        }
    }
}
