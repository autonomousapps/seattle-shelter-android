package com.seattleshelter.core.transformers

import com.seattleshelter.entities.PetType
import com.seattleshelter.db.entities.Pet as DbPet
import com.seattleshelter.entities.Pet as NetPet

fun dbToNet(db: DbPet) = NetPet(
    id = db.id,
    name = db.name,
    picUrl = db.picUrl,
    sex = db.sex,
    breed = db.breed,
    age = db.age,
    inFosterCare = db.inFosterCare,
    adoptionPending = db.adoptionPending
)

fun List<DbPet>.toNetList() = map { dbToNet(it) }

fun netToDb(net: NetPet, petType: PetType) = DbPet(
    id = net.id,
    petType = petType,
    name = net.name,
    picUrl = net.picUrl,
    sex = net.sex,
    breed = net.breed,
    age = net.age,
    inFosterCare = net.inFosterCare,
    adoptionPending = net.adoptionPending
)

fun List<NetPet>.toDbList(petType: PetType) = map { netToDb(it, petType) }
