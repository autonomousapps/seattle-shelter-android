package com.seattleshelter.core.api

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PetsService {
    fun getCatsList(): Single<List<Pet>>
    fun getCatById(id: Long): Single<PetDetails>

    fun getDogsList(): Single<List<Pet>>
    fun getDogById(id: Long): Single<PetDetails>

    fun getRabbitsList(): Single<List<Pet>>
    fun getRabbitById(id: Long): Single<PetDetails>

    fun getSmallMammalsList(): Single<List<Pet>>
    fun getSmallMammalById(id: Long): Single<PetDetails>

    fun getReptilesList(): Single<List<Pet>>
    fun getReptileById(id: Long): Single<PetDetails>
}

internal class PetsServiceImpl(
    private val service: InnerPetsService
) : PetsService {

    override fun getCatsList(): Single<List<Pet>> = service.getCatsList()
    override fun getCatById(id: Long): Single<PetDetails> = service.getCatById(id)

    override fun getDogsList(): Single<List<Pet>> = service.getDogsList()
    override fun getDogById(id: Long): Single<PetDetails> = service.getDogById(id)

    override fun getRabbitsList(): Single<List<Pet>> = service.getRabbitsList()
    override fun getRabbitById(id: Long): Single<PetDetails> = service.getRabbitById(id)

    override fun getSmallMammalsList(): Single<List<Pet>> = service.getSmallMammalsList()
    override fun getSmallMammalById(id: Long): Single<PetDetails> = service.getSmallMammalById(id)

    override fun getReptilesList(): Single<List<Pet>> = service.getReptilesList()
    override fun getReptileById(id: Long): Single<PetDetails> = service.getReptileById(id)
}

internal interface InnerPetsService {

    @GET("cats")
    fun getCatsList(): Single<List<Pet>>

    @GET("cats/{id}")
    fun getCatById(@Path("id") id: Long): Single<PetDetails>

    @GET("dogs")
    fun getDogsList(): Single<List<Pet>>

    @GET("dogs/{id}")
    fun getDogById(@Path("id") id: Long): Single<PetDetails>

    @GET("rabbits")
    fun getRabbitsList(): Single<List<Pet>>

    @GET("rabbits/{id}")
    fun getRabbitById(@Path("id") id: Long): Single<PetDetails>

    @GET("smallmammals")
    fun getSmallMammalsList(): Single<List<Pet>>

    @GET("smallmammals/{id}")
    fun getSmallMammalById(@Path("id") id: Long): Single<PetDetails>

    @GET("reptiles")
    fun getReptilesList(): Single<List<Pet>>

    @GET("reptiles/{id}")
    fun getReptileById(@Path("id") id: Long): Single<PetDetails>
}
