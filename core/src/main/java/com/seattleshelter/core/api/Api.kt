package com.seattleshelter.core.api

import android.util.Log
import com.seattleshelter.db.moshiConverterFactory
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit

object Api {

    fun newPetsService(): PetsService = PetsServiceImpl(createService())

    private val retrofit = newRetrofit()

    private fun newRetrofit(): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor { Log.v("Api", "Pet: $it") }
            .apply { level = HttpLoggingInterceptor.Level.BODY }
        val client = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .callTimeout(30, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .client(client)
            //.baseUrl("http://192.168.1.102:5050") // used when API is deployed locally
            .baseUrl("https://seattleanimalshelter.herokuapp.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(moshiConverterFactory())
            .build()
    }

    private inline fun <reified T> createService(): T {
        return retrofit.create(T::class.java)
    }
}