package com.seattleshelter.core

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.seattleshelter.core.api.PetsService
import com.seattleshelter.core.base.DisposingViewModel
import com.seattleshelter.entities.PetDetails

class PetDetailViewModel(
    private val petsService: PetsService,
    private val id: Long
) : DisposingViewModel() {

    private val _avatarUrl = MutableLiveData<String>()

    private val _petDetails = MutableLiveData<PetDetails>().also { data ->
        // TODO: huh, apparently the ID endpoint doesn't matter. I can get any species from this one.
        petsService.getCatById(id)
            .doOnSubscribe { disposables.add(it) }
            .subscribe(
                {
                    data.postValue(it)
                    _avatarUrl.postValue(it.picUrls.firstOrNull())
                },
                { Log.e("PetDetailViewModel", "Error: ${it.localizedMessage}", it) }
            )

        // TODO persist in a database
    }

    fun catDetails(): LiveData<PetDetails> = _petDetails

    fun avatarUrl(): LiveData<String> = _avatarUrl

    /**
     * 0-based.
     */
    fun onPictureSelected(picNum: Int) {
        _petDetails.value?.let {
            _avatarUrl.value = it.picUrls[picNum]
        }
    }
}
