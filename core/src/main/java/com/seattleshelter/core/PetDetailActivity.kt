package com.seattleshelter.core

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.seattleshelter.core.api.PetsService
import com.seattleshelter.core.base.BaseActivity
import com.seattleshelter.core.di.Id
import com.seattleshelter.entities.PetDetails
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Inject

class PetDetailActivity : BaseActivity() {

    @Inject lateinit var vmFactory: PetDetailActivityViewModelFactory
    private val viewModel by lazy {
        ViewModelProviders.of(this, vmFactory).get(PetDetailViewModel::class.java)
    }

    internal val id by lazy { intent.getLongExtra(ARG_CAT_ID, -1L) }

    private val name by lazy { findViewById<TextView>(R.id.name) }
    private val description by lazy { findViewById<TextView>(R.id.description) }
    private val avatar by lazy { findViewById<ImageView>(R.id.avatar) }
    private val button1 by lazy { findViewById<Button>(R.id.button1) }
    private val button2 by lazy { findViewById<Button>(R.id.button2) }
    private val button3 by lazy { findViewById<Button>(R.id.button3) }
    private val video by lazy { findViewById<Button>(R.id.video) }
    private val progressBar by lazy { findViewById<ContentLoadingProgressBar>(R.id.progressBar) }

    private var pet: PetDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_cat_detail)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.catDetails().observe(this, Observer { details ->
            Log.d(TAG, "pet details = $details")
            details?.let {
                pet = it
                setDetails(it)
                progressBar.hide()
            }
        })

        viewModel.avatarUrl().observe(this, Observer { avatarUrl ->
            Log.d(TAG, "avatarUrl = $avatarUrl")
            avatarUrl?.let { setAvatar(avatarUrl) }
        })

        button1.setOnClickListener { viewModel.onPictureSelected(0) }
        button2.setOnClickListener { viewModel.onPictureSelected(1) }
        button3.setOnClickListener { viewModel.onPictureSelected(2) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_pet, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d(TAG, "onOptionsItemSelected")
        when (item.itemId) {
            R.id.menu_share -> {
                sharePet()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // TODO would be nice if picture showed up in Twitter preview
    private fun sharePet() {
        Log.d(TAG, "sharePet")
        pet?.let {
            Log.d(TAG, "sharing $it")
            val picUrl = if (it.picUrls.isNotEmpty()) {
                "\n\n${it.picUrls[0]}"
            } else {
                ""
            }
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT,
                    "Adopt ${it.name} from the Seattle Animal Shelter!$picUrl"
                )
                type = "text/plain"
            }
            startActivity(Intent.createChooser(sendIntent, resources.getText(R.string.share)))
        } ?: Log.w(TAG, "Nothing to share")
    }

    private fun setDetails(details: PetDetails) {
        name.text = details.name
        description.text = if (details.description?.isNotBlank() == true) {
            details.description
        } else {
            "No description"
        }

        when {
            details.picUrls.size == 3 -> {
                button1.visibility = View.VISIBLE
                button2.visibility = View.VISIBLE
                button3.visibility = View.VISIBLE
            }
            details.picUrls.size == 2 -> {
                button1.visibility = View.VISIBLE
                button2.visibility = View.VISIBLE
                button3.visibility = View.GONE
            }
            else -> {
                button1.visibility = View.GONE
                button2.visibility = View.GONE
                button3.visibility = View.GONE
            }
        }

        if (details.videoUrl.isNullOrBlank()) {
            video.visibility = View.GONE
        } else {
            video.setOnClickListener {
                Intent(Intent.ACTION_VIEW)
                    .apply { data = details.videoUrl!!.toUri() }
                    .run { startActivity(this) }
            }
            video.visibility = View.VISIBLE
        }
    }

    private fun setAvatar(avatarUrl: String) {
        Log.d(TAG, "Loading details pic $avatarUrl")
        if (avatarUrl.isNotBlank()) {
            Picasso.get().load(avatarUrl).into(avatar)
        }
    }

    companion object {

        private const val TAG = "PetDetailActivity"
        private const val ARG_CAT_ID = "arg_cat_id"

        fun getStartIntent(context: Context, id: Long): Intent =
            Intent(context, PetDetailActivity::class.java).apply {
                putExtra(ARG_CAT_ID, id)
            }
    }
}

@Module object PetDetailActivityModule {
    @JvmStatic @Provides @Id fun provideId(activity: PetDetailActivity): Long = activity.id
}

class PetDetailActivityViewModelFactory @Inject constructor(
    private val petsService: PetsService,
    @Id private val id: Long
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(PetDetailViewModel::class.java) -> {
                PetDetailViewModel(petsService, id) as T
            }
            else -> throw IllegalArgumentException(
                "${modelClass.simpleName} is an unknown type of view model"
            )
        }
    }
}
