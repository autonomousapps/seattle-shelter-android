package com.seattleshelter.core

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.seattleshelter.core.api.PetsService
import com.seattleshelter.core.base.DisposingViewModel
import com.seattleshelter.core.transformers.toDbList
import com.seattleshelter.core.transformers.toNetList
import com.seattleshelter.db.daos.PetDao
import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetType
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PetsViewModel(
    private val petDao: PetDao,
    private val petsService: PetsService
) : DisposingViewModel() {

    private var petType: PetType = PetType.CATS

    private val petsListData = MutableLiveData<List<Pet>>()

    private lateinit var netSubscription: Disposable
    private lateinit var dbSubscription: Disposable

    private var ordering = { pet: Pet -> pet.name }

    init {
        subscribeToDb()
        subscribeToApi()
    }

    fun petsList(): LiveData<List<Pet>> = petsListData

    fun setPetType(petType: PetType) {
        if (this.petType != petType) {
            this.petType = petType

            netSubscription.dispose()
            dbSubscription.dispose()

            subscribeToDb()
            subscribeToApi()
        }
    }

    private fun subscribeToDb() {
        dbSubscription = petDao.getPetsFor(petType).toObservable()
            .doOnSubscribe { disposables.add(it) }
            .subscribeOn(Schedulers.io())
            .subscribe(
                { petsListData.postValue(it.toNetList()) },
                { Log.e(TAG, "Error getting $petType from db", it) }
            )
    }

    private fun subscribeToApi() {
        netSubscription = getPetsList()
            .doOnSubscribe { disposables.add(it) }
            .map { it.sortedBy(ordering) }
            .subscribe(
                { petDao.replacePets(it.toDbList(petType)) },
                { Log.e(TAG, "Error getting $petType: ${it.localizedMessage}", it) }
            )
    }

    private fun getPetsList() = when (petType) {
        PetType.CATS -> petsService.getCatsList()
        PetType.DOGS -> petsService.getDogsList()
        PetType.RABBITS -> petsService.getRabbitsList()
        PetType.SMALL_MAMMALS -> petsService.getSmallMammalsList()
        PetType.REPTILES -> petsService.getReptilesList()
    }

    companion object {
        private const val TAG = "PetsViewModel"
    }
}
