package com.seattleshelter.core

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seattleshelter.entities.Pet

class PetAdapter(
    private val clickListener: (Long) -> Unit
) : RecyclerView.Adapter<PetViewHolder>() {

    private val pets = mutableListOf<Pet>()

    fun setPets(newPets: List<Pet>) {
        pets.clear()
        pets.addAll(newPets)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = pets.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pet, parent, false)
        return PetViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(viewHolder: PetViewHolder, position: Int) {
        viewHolder.bind(pets[position])
    }
}
