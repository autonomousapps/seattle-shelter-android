plugins {
    id("com.gradle.enterprise") version "3.3.4"
}

gradleEnterprise {
    buildScan {
        publishAlways()
        termsOfServiceUrl = "https://gradle.com/terms-of-service"
        termsOfServiceAgree = "yes"
    }
}

rootProject.name = "seattle-shelter-android"

include("app")
include("core")
include("db")
include("entities")
include("platform")
