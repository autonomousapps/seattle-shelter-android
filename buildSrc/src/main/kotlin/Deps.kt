import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.exclude

object Versions {
    const val archx = "2.0.0"
    const val room = "2.2.0-alpha01"
    const val moshi = "1.8.0"
    const val retrofit = "2.5.0"
    const val okhttp = "3.12.0"
    const val okio = "1.16.0"
    const val rxjava = "2.2.4"
    const val rxandroid = "2.1.0"
    const val rxkotlin = "2.3.0"
    const val dagger = "2.24"
}

@Suppress("unused")
object Deps {
    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"

    // Arch components
    const val archExtensionsX = "androidx.lifecycle:lifecycle-extensions:${Versions.archx}"
    const val archCommonX = "androidx.lifecycle:lifecycle-common-java8:${Versions.archx}"
    const val archReactiveX = "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.archx}"

    // Arch-room
    const val roomRuntimeX = "androidx.room:room-runtime:${Versions.room}"
    const val roomCompilerKaptX = "androidx.room:room-compiler:${Versions.room}"
    const val roomRxJavaX = "androidx.room:room-rxjava2:${Versions.room}"
    const val roomTestingX = "androidx.room:room-testing:${Versions.room}"

    // Retrofit/OkHttp
    const val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    const val moshiAdapters = "com.squareup.moshi:moshi-adapters:${Versions.moshi}"
    const val moshiKotlin = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitConverterMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val retrofitAdapterRxJava = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
    const val okio = "com.squareup.okio:okio:${Versions.okio}"

    // Rx
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxandroid}"
    const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxkotlin}"

    const val picasso = "com.squareup.picasso:picasso:2.71828"

    // Dagger
    const val dagger = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerKapt = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val daggerSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerSupportKapt = "com.google.dagger:dagger-compiler:${Versions.dagger}"
}

fun DependencyHandlerScope.kotlin() {
    "implementation"(Deps.kotlinStdlib)
}

fun DependencyHandlerScope.moshi() {
    "implementation"(Deps.moshi)
    "implementation"(Deps.moshiAdapters)
    "implementation"(Deps.retrofitConverterMoshi)
    "implementation"(Deps.moshiKotlin)
}

fun DependencyHandlerScope.dagger() {
    "implementation"(Deps.dagger)
    "implementation"(Deps.daggerSupport)
    "kapt"(Deps.daggerKapt)
    "kapt"(Deps.daggerSupportKapt)
    "kaptAndroidTest"(Deps.daggerKapt)
    "kaptAndroidTest"(Deps.daggerSupportKapt)
}

fun DependencyHandlerScope.retrofit() {
    "implementation"(Deps.moshi)
    "implementation"(Deps.moshiAdapters)
    "implementation"(Deps.retrofit)
    "implementation"(Deps.retrofitConverterMoshi)
    "implementation"(Deps.retrofitAdapterRxJava)
    "implementation"(Deps.okhttp)
    "implementation"(Deps.okhttpLogging)
    "implementation"(Deps.okio)
}

fun DependencyHandlerScope.archX() {
    "implementation"(Deps.archExtensionsX)
    "implementation"(Deps.archCommonX)
    "implementation"(Deps.archReactiveX)
}

fun DependencyHandlerScope.roomX() {
    "api"(Deps.roomRuntimeX) {
        because("The supertype is part of the API")
    }
    "implementation"(Deps.roomRxJavaX)
    "kapt"(Deps.roomCompilerKaptX)
}

fun DependencyHandlerScope.rxJava() {
    "implementation"(Deps.rxJava)
    "implementation"(Deps.rxAndroid)
    "implementation"(Deps.rxKotlin)
}

fun DependencyHandlerScope.picasso() {
    "implementation"(Deps.picasso) {
        exclude(module = "exifinterface")
    }
}
