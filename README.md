# TODO
1. Add sharing link back to Seattle Animal Shelter site.
1. Add support for sorting/filtering list.
   1. Age, sex, foster/not.
   1. Sort by most recent (implies smart local storage)
   1. Bonded pairs?
1. Consider use of SqlDelite vs Room.
1. Consider use of coroutines instead of RxJava
