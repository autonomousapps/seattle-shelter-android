plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(platform(project(":platform")))

    implementation(project(":entities"))

    archX()
    roomX()

    // old moshi()
    implementation(Deps.moshi)
    implementation(Deps.moshiAdapters)
    implementation(Deps.retrofitConverterMoshi) // compileOnly?
    implementation(Deps.moshiKotlin)

    implementation("androidx.appcompat:appcompat")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("junit:junit")

    androidTestImplementation("androidx.test:runner")
    androidTestImplementation("androidx.test.espresso:espresso-core")
}
