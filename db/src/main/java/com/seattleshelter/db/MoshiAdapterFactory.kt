package com.seattleshelter.db

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

/**
 * For use with Retrofit, to enable it to deserialize JSON responses using Moshi. Use it like so:
 *
 * ```
 * Retrofit retrofit = new Retrofit.Builder()
 *         .addConverterFactory(moshiConverterFactory())
 *         .build();
 * ```
 *
 * @return a [MoshiConverterFactory] for use by Retrofit.
 */
fun moshiConverterFactory(): MoshiConverterFactory {
    return MoshiConverterFactory.create(getMoshi())
}

fun getMoshi(): Moshi = Moshi.Builder()
    .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
    .add(KotlinJsonAdapterFactory())
//    .add(EntitiesJsonAdapterFactory.INSTANCE)
//    .add(TypeAdapters())
    .build()

inline fun <reified T> getJsonAdapter(): JsonAdapter<T> {
    return getMoshi().adapter(T::class.java)
}

inline fun <reified T> String.fromJson(): T? {
    return try {
        getJsonAdapter<T>().fromJson(this)
    } catch (e: JsonDataException) {
        e.printStackTrace()
        null
    }
}

inline fun <reified T> T.toJson(): String {
    return getJsonAdapter<T>().toJson(this)
}
