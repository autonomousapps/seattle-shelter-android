package com.seattleshelter.db.daos

import android.util.Log
import androidx.room.*
import com.seattleshelter.db.ShelterDatabase
import com.seattleshelter.db.entities.Pet
import com.seattleshelter.entities.PetType
import io.reactivex.Flowable

@Dao
abstract class PetDao {

    @Query("SELECT * FROM ${ShelterDatabase.TABLE_PETS} WHERE petType = :petType")
    abstract fun getPetsFor(petType: PetType): Flowable<List<Pet>>

    @Transaction
    open fun replacePets(pets: List<Pet>) {
        if (pets.isNotEmpty()) {
            val type = pets[0].petType
            deleteAllPetsFor(type)
            insertPets(pets)
        } else {
            Log.w("PetDao", "replacePets called with an empty list. Doing nothing.")
        }
    }

    @Query("DELETE FROM ${ShelterDatabase.TABLE_PETS} WHERE petType = :petType")
    internal abstract fun deleteAllPetsFor(petType: PetType)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    internal abstract fun insertPets(cat: List<Pet>)
}