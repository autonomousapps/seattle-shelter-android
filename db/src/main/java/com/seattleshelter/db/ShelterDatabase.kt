package com.seattleshelter.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.seattleshelter.db.daos.PetDao
import com.seattleshelter.db.entities.Pet

@Database(entities = [
    Pet::class
], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ShelterDatabase : RoomDatabase() {

    companion object {

        private const val DATABASE_NAME = "shelter-database"

        internal const val TABLE_PETS = "pets"
        internal const val TABLE_PET_DETAILS = "pet_details"

        fun init(context: Context): ShelterDatabase {
            return buildDatabase(context)
        }

        private fun buildDatabase(context: Context): ShelterDatabase {
            return Room.databaseBuilder(context, ShelterDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }

    abstract fun petDao(): PetDao
}
