package com.seattleshelter.db

import androidx.room.TypeConverter
import com.seattleshelter.entities.PetType

internal object Converters {

    @TypeConverter @JvmStatic fun fromStringList(list: List<String>): String = list.toJson()
    @TypeConverter @JvmStatic fun toStringList(json: String): List<String> = json.fromJson<List<String>>() ?: emptyList()

    @TypeConverter @JvmStatic fun fromPetType(petType: PetType): String = petType.name
    @TypeConverter @JvmStatic fun toPetType(petType: String): PetType = PetType.fromString(petType)
}
