package com.seattleshelter.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.seattleshelter.db.ShelterDatabase
import com.seattleshelter.entities.PetType

@Entity(tableName = ShelterDatabase.TABLE_PET_DETAILS)
class PetDetails(
    @PrimaryKey val id: Long,
    val petType: PetType,
    val name: String,
    val picUrls: List<String>,
    val videoUrl: String?,
    val description: String,
    val sex: String?,
    val breed: String?,
    val age: String?,
    val size: String?,
    val color: String?,
    val altered: Boolean?,
    val declawed: Boolean,
    val inFosterCare: Boolean,
    val adoptionPending: Boolean
)
