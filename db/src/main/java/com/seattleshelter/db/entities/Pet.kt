package com.seattleshelter.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.seattleshelter.db.ShelterDatabase
import com.seattleshelter.entities.PetType

@Entity(tableName = ShelterDatabase.TABLE_PETS)
class Pet(
    @PrimaryKey val id: Long,
    val petType: PetType,
    val name: String,
    val picUrl: String,
    val sex: String,
    val breed: String,
    val age: String,
    val inFosterCare: Boolean,
    val adoptionPending: Boolean
)
