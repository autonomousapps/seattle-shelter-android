package com.seattleshelter

import android.app.Application
import android.content.Context
import com.seattleshelter.core.MainActivity
import com.seattleshelter.core.PetDetailActivity
import com.seattleshelter.core.PetDetailActivityModule
import com.seattleshelter.core.api.Api
import com.seattleshelter.core.api.PetsService
import com.seattleshelter.db.ShelterDatabase
import com.seattleshelter.db.daos.PetDao
import com.squareup.picasso.Picasso
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject
import javax.inject.Singleton

@Suppress("unused")
open class MainApplication : Application(), HasAndroidInjector {

    @Inject protected lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initPicasso()
    }

    private fun initDagger() {
        DaggerMainApplicationComponent.builder()
            .app(this)
            .build()
            .inject(this)
    }

    private fun initPicasso() {
        Picasso.setSingletonInstance(
            Picasso.Builder(applicationContext).loggingEnabled(false).build()
        )
    }
}

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ScreenBindingModule::class,
    ApiModule::class,
    DatabaseModule::class
])
interface MainApplicationComponent {
    fun inject(app: MainApplication)

    @Component.Builder
    interface Builder {
        fun build(): MainApplicationComponent
        @BindsInstance fun app(app: Context): Builder
    }
}

@Module abstract class ScreenBindingModule {
    @ContributesAndroidInjector//(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [PetDetailActivityModule::class])
    abstract fun catDetailActivity(): PetDetailActivity
}

@Module object ApiModule {
    @Provides @JvmStatic fun petsService(): PetsService = Api.newPetsService()
}

@Module object DatabaseModule {

    @Singleton @Provides @JvmStatic fun database(context: Context): ShelterDatabase {
        return ShelterDatabase.init(context)
    }

    @Provides @JvmStatic fun petDao(db: ShelterDatabase): PetDao = db.petDao()
}
