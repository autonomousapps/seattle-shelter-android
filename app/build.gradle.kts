plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        applicationId = "com.seattleshelter"
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {
    implementation(platform(project(":platform")))

    implementation(project(":core"))
    implementation(project(":db"))
    implementation(project(":entities"))

    dagger()

    implementation("androidx.annotation:annotation")
    implementation("androidx.appcompat:appcompat")
    implementation("androidx.core:core-ktx")
    implementation("com.squareup.picasso:picasso") {
        exclude(module = "exifinterface")
    }
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
