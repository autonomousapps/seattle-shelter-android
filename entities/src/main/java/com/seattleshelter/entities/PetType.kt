package com.seattleshelter.entities

import java.util.*
import kotlin.NoSuchElementException

enum class PetType {
    CATS, DOGS, RABBITS, SMALL_MAMMALS, REPTILES;

    companion object {
        fun fromString(name: String): PetType = when (name.toLowerCase(Locale.ROOT)) {
            "cats" -> CATS
            "dogs" -> DOGS
            "rabbits" -> RABBITS
            "small mammals", "small_mammals" -> SMALL_MAMMALS
            "reptiles" -> REPTILES
            else -> throw NoSuchElementException("$name is not a member of Pets")
        }
    }
}