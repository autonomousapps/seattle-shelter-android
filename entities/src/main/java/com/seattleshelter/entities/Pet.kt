package com.seattleshelter.entities

data class Pet(
    val name: String,
    val picUrl: String,
    val id: Long,
    val sex: String,
    val breed: String,
    val age: String,
    val inFosterCare: Boolean,
    val adoptionPending: Boolean
)
