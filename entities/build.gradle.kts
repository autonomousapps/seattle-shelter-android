import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm")
}

dependencies {
    implementation(platform(project(":platform")))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
